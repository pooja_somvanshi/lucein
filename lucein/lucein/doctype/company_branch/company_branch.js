// Copyright (c) 2017, sagar.s@indictranstech.com and contributors
// For license information, please see license.txt
{% include 'erpnext/selling/sales_common.js' %};
// frappe.provide("erpnext.queries");
frappe.ui.form.on('Company Branch', {
	refresh: function(frm) {
	
	},
	// branch_address: function() {
	// 	erpnext.utils.get_address_display(cur_frm, "branch_address");
	// },
	validate:function(frm) {
	}
});

cur_frm.fields_dict['branch_address'].get_query = function(doc) {
	return{
		filters:[
			['Address', 'address_title', '=', cur_frm.doc.name]
		]
	}
}

